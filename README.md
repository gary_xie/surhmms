# surHMMs
**surHMMs: Shortest Unique Representative Hidden Markov Models**
surHMMs is an approach to generates unique motif sequences for HMM profiling searching, resulting in higher specificity and sensitivity.  Since it combines the best parts of ShortBRED (Short better representative extract dataset) and the hidden Markov model (HMM), the surHMM can identify distant homologs through searching unique motifs that still have been preserved.  Searches against the surHMMs database are also more accurate and faster, as the exclusion of non-specific (overlap) regions reduces false positive hits and the search space is considerably reduced relative to the full database.  The surHMMs approach can apply to other homology-based search tasks, such as determine the presence and abundance of the protein families of interest.
For more information on the technical aspects to this approach, or to cite surHMMs, please use the following citation:

**Installing surHMMs**
Since surHMMs are all based on off-shelf open sourced software, to install and run surHMMs, it needs to install the following dependencies:
* Python 2.7.9
* Biopython v1.65
* ncbi-blast-2.2.28+
* usearch v6.0.307 
* MUSCLE v3.8.31
* CD-HIT version 4.6
* ShortBRED 2016-01
* Hmmer v.3.3


**Program Structure**
consists of two parts:
*  ShortBRED-Identify - This takes a FASTA file of amino acid sequences, searches for overlap among itself and against a separate reference file of amino acid sequences, and then produces a FASTA file of markers.

*  Hmmer-identify - This takes the FASTA file of markers and generates its corresponding HMM models, then quantifies their relative abundance in a protein FASTA file or identity its remote homologs in genome protein sequences.
 
**Running ShortBRED Identify to Create Marker Unique Consensus Sequence**

*  Method 1: Create new markers from a set of proteins of interest and a reference set of proteins.
./shortbred_identify.py --goi  example/input_prots.faa --ref example/ref_prots.faa --markers mytestmarkers.faa --tmp example_identify 

*  Method 2: Create ShortBRED markers using a set of proteins of interest, and a blast database of reference proteins.
./shortbred_identify.py --goi  example/input_prots.faa --refdb example_identify/refdb/refdb --markers mytestmarkers_prebuiltdb.faa --tmp example_identify_prebuiltdb

*  Method 3: Create a new set of ShortBRED markers using a previous set of blast search results.
./shortbred_identify.py --goiclust  example_identify/clust/clust.faa --goiblast example_identify/blastresults/selfblast.txt --refblast example_identify/blastresults/refblast.txt --map_in example_identify/clust/clust.map --markers mytestmarkers_previous_search_results.faa

For more detail, check in ShortBRED wiki page:
https://bitbucket.org/biobakery/shortbred/wiki/Home

**Running Hmmer to convert Consensus Sequences to HMMs, then Profile Proteins of Interest and Identify Remote Homologs.**

Common steps: Convert marker unique Consensus Sequences to HMMs

*  phmmer — search protein sequence(s) against a protein sequence database
    phmmer [options] -A alignment_output  <seqfile> <seqdb>
    https://www.mankier.com/1/phmmer

*  hmmbuild — construct profile HMM(s) from multiple sequence alignment(s)
    hmmbuild [options]  -o summary_output --amino  <hmmfile_out> <msafile>
    https://www.mankier.com/1/hmmbuild#Synopsis

*  Method 1: Identify remote homologs from submitted protein sequences using hmmsearch and a set of markers.
hmmsearch — search profile(s) against a sequence database. Output ranked lists of the sequences with the most significant matches to the profile.
    hmmsearch [options] --tblout output_table -E e-value-cutoff <hmmfile> <seqdb>
    https://www.mankier.com/1/hmmsearch

*  Method 2: Profile markers in submitted protein sequences using hmmscan and a set of markers. Output ranked lists of the profiles with the most significant matches to the sequence. 
1.	hmmpress — prepare an HMM database for hmmscan 
    hmmpress [options] <hmmfile>
    https://www.mankier.com/1/hmmpress
2.	hmmscan — search protein sequence(s) against a protein profile database
    hmmscan [options] –tblout table_output -E e-value-cutoff  <hmmdb> <seqfile> 
    https://www.mankier.com/1/hmmscan
    Tip: table_output can be further easily visualized by using PivotTable function of MS Excel

Downloadable pre-computed surHMMs profiles
* Vibrio cholerae toxin_toxin_vfdb.hmm
* Clostridium perfringens Epsilon toxin_vfdb.hmm
* Clostridium botulinum neurotoxin_vfdb.hmm
* Shiga-like toxin_vfdb.hmm
* Staphylococcal enterotoxin B_vfdb.hmm
* VFDB_2017.hmm
* CARD_small_homolog_model.hmm

